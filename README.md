# First tkinter App

## Getting started

This is a project where I'm just going to post a test application while I learn to use tkinter to create GUI Apps with python. I'm using the codemy/freecodecamp training course: [Python GUI's With TKinter](https://www.youtube.com/playlist?list=PLCC34OHNcOtoC6GglhF3ncJ5rLwQrLGnV)

## Authors and acknowledgment
Coded by Eduard Lucena (@x3mboy) and the training is from John Elder, founder of [codemy.com](https://codemy.com/)

## License
This is licensed with GPLv3. Please check the LICENSE file in the project.

## Project status
Work in progress
